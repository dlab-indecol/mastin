import os
import sys
import unittest

from pydantic import ValidationError

TESTPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(TESTPATH, '../mastin'))

from mastin.ships import create_ship, create_empty_ship, \
    create_standard_ship  # noqa
from mastin.models.ship import ShipType  # noqa


ship_fields = ["mmsi",
               "imo_number",
               "name",
               "aux_engine_kw",
               "aux_engine_stroke",
               "breadth",
               "built",
               "draught",
               "dwt",
               "fuel_type",
               "gross",
               "ldt",
               "length",
               "length_bp",
               "main_engine_cyl",
               "main_engine_kw",
               "main_engine_model",
               "main_engine_rpm",
               "main_engine_stroke",
               "propulsion",
               "reefer_points",
               "service_speed",
               "ship_type",
               "ship_type_detailed",
               "teu",
               "filler",
               "note"]


class TestStringMethods(unittest.TestCase):
    def test_create_ship_empty(self):
        ship = create_ship()

        for item in ship_fields:
            if item == 'mmsi':
                # must start with 999
                self.assertEqual(int(str(ship.__getattribute__(item))[:3]),
                                 999)
            else:
                self.assertIsNone(ship.__getattribute__(item))

    def test_create_ship_with_fields(self):
        ship = create_ship(ship_type='container', length=1)

        for item in ship_fields:
            if item == 'mmsi':
                # must start with 999
                self.assertEqual(int(str(ship.__getattribute__(item))[:3]),
                                 999)
            elif item == 'ship_type':
                self.assertEqual(ship.__getattribute__(item),
                                 'container')
            elif item == 'length':
                self.assertEqual(ship.__getattribute__(item), 1)
            else:
                self.assertIsNone(ship.__getattribute__(item))

    def test_create_ship_mmsi_warning(self):
        self.assertWarns(UserWarning, create_ship, mmsi=1)

    def test_create_ship_str_to_num_validation_error(self):
        # ship = create_ship(length='some string')
        self.assertRaises(ValidationError, create_ship, length='some string')

    def test_create_empty_ship(self):
        ship = create_empty_ship()
        for item in ship_fields:
            if item == 'mmsi':
                # must start with 999
                self.assertEqual(int(str(ship.__getattribute__(item))[:3]),
                                 999)
            else:
                self.assertIsNone(ship.__getattribute__(item))

    def test_create_standard_ship(self):
        self.assertRaises(NotImplementedError, create_standard_ship,
                          ship_type='container')


if __name__ == '__main__':
    unittest.main()

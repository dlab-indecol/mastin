import os
import sys
from unittest.mock import patch
from nose.tools import assert_equal

from tests.mocks import get_free_port, start_mock_server


TESTPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(TESTPATH, '../mastin'))

from mastin.services.auth import access_token, login  # noqa


class TestMockServer(object):
    @classmethod
    def setup_class(cls):
        cls.mock_server_port = get_free_port()
        start_mock_server(cls.mock_server_port)

    def test_request_response(self):
        mock_ships_url = \
            f'http://localhost:{self.mock_server_port}/login/access-token'

        # Patch TOKEN_URL so that the service uses the mock
        # server URL instead of the real URL.
        with patch.dict('mastin.services.auth.__dict__',
                        {'TOKEN_URL': mock_ships_url}):
            login('doctor', 'who')
            response = access_token()

        assert_equal(response, 'token')

from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import re
import socket
from threading import Thread

import requests


class MockServerRequestHandler(BaseHTTPRequestHandler):
    AUTH_PATTERN = re.compile(r'/login/access-token')
    SHIPS_PATTERN = re.compile(r'/ships')

    def do_POST(self):  # noqa
        if re.search(self.AUTH_PATTERN, self.path):
            # Add response status code.
            self.send_response(requests.codes.ok)

            # Add response headers.
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            # Add response content.
            response_content = json.dumps({
                "access_token": "token",
                "token_type": "bearer"
            })
            self.wfile.write(response_content.encode('utf-8'))
            return
        elif re.search(self.SHIPS_PATTERN, self.path):
            self.send_response(requests.codes.ok)
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            ship = self.rfile.read(int(self.headers.get('Content-Length')))
            mmsi = json.loads(ship)['mmsi']
            response_content = json.dumps(
                    {
                        "mmsi": mmsi
                    }
                )
            self.wfile.write(response_content.encode('utf-8'))
            return

    def do_GET(self):  # noqa
        if re.search(self.SHIPS_PATTERN, self.path):
            self.send_response(requests.codes.ok)
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()
            response_content = json.dumps(
                [
                    {
                        "mmsi": 0,
                        "imo_number": 0,
                        "name": "string",
                        "aux_engine_kw": 0,
                        "aux_engine_stroke": 0,
                        "breadth": 0,
                        "built": "string",
                        "draught": 0,
                        "dwt": 0,
                        "fuel_type": "string",
                        "gross": 0,
                        "ldt": 0,
                        "length": 0,
                        "length_bp": 0,
                        "main_engine_cyl": 0,
                        "main_engine_kw": 0,
                        "main_engine_model": "string",
                        "main_engine_rpm": 0,
                        "main_engine_stroke": 0,
                        "propulsion": "string",
                        "reefer_points": 0,
                        "service_speed": 0,
                        "ship_type": "string",
                        "ship_type_detailed": "string",
                        "teu": 0,
                        "filler": "string",
                        "note": "string",
                        "owner_id": 0
                    }
                ])
            self.wfile.write(response_content.encode('utf-8'))
            return

    def do_DELETE(self):
        if re.search(self.SHIPS_PATTERN, self.path):
            self.send_response(requests.codes.ok)
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            mmsi = int(self.requestline.split('/')[2].split(' ')[0])
            response_content = json.dumps(
                {
                    "mmsi": mmsi
                }
            )
            self.wfile.write(response_content.encode('utf-8'))
            return


def get_free_port():
    s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    address, port = s.getsockname()
    s.close()
    return port


def start_mock_server(port):
    mock_server = HTTPServer(('localhost', port), MockServerRequestHandler)
    mock_server_thread = Thread(target=mock_server.serve_forever)
    mock_server_thread.setDaemon(True)
    mock_server_thread.start()

import os
import sys
from unittest.mock import patch
from nose.tools import assert_equal
import json

from tests.mocks import get_free_port, start_mock_server

TESTPATH = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(TESTPATH, '../mastin'))

from mastin.services.ships import get_ships, create_ship, delete_ship  # noqa

ship = {
    "mmsi": 0,
    "imo_number": 0,
    "name": "string",
    "aux_engine_kw": 0,
    "aux_engine_stroke": 0,
    "breadth": 0,
    "built": "string",
    "draught": 0,
    "dwt": 0,
    "fuel_type": "string",
    "gross": 0,
    "ldt": 0,
    "length": 0,
    "length_bp": 0,
    "main_engine_cyl": 0,
    "main_engine_kw": 0,
    "main_engine_model": "string",
    "main_engine_rpm": 0,
    "main_engine_stroke": 0,
    "propulsion": "string",
    "reefer_points": 0,
    "service_speed": 0,
    "ship_type": "string",
    "ship_type_detailed": "string",
    "teu": 0,
    "filler": "string",
    "note": "string",
    "owner_id": 0
}


class TestMockServer(object):
    @classmethod
    def setup_class(cls):
        cls.mock_server_port = get_free_port()
        start_mock_server(cls.mock_server_port)

    def test_get_ship(self):
        mock_ships_url = \
            f'http://localhost:{self.mock_server_port}/ships'

        # Patch TOKEN_URL so that the service uses the mock
        # server URL instead of the real URL.
        with patch.dict('mastin.services.ships.__dict__',
                        {'SHIPS_URL': mock_ships_url}):
            response = get_ships('token')

        content = json.loads(response.text)

        for item in content[0]:
            assert_equal(ship[item], content[0][item])

    def test_create_ship(self):
        mock_ships_url = \
            f'http://localhost:{self.mock_server_port}/ships'

        # Patch TOKEN_URL so that the service uses the mock
        # server URL instead of the real URL.
        with patch.dict('mastin.services.ships.__dict__',
                        {'SHIPS_URL': mock_ships_url}):
            response = create_ship(access_token='token', ship_in=ship)

        content = json.loads(response.text)

        assert_equal(ship['mmsi'], content['mmsi'])

    def test_delete_ship(self):
        mock_ships_url = \
            f'http://localhost:{self.mock_server_port}/ships'

        # Patch TOKEN_URL so that the service uses the mock
        # server URL instead of the real URL.
        with patch.dict('mastin.services.ships.__dict__',
                        {'SHIPS_URL': mock_ships_url}):
            response = delete_ship(access_token='token', mmsi=12)

        content = json.loads(response.text)

        assert_equal(content['mmsi'], 12)

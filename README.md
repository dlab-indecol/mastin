### MariTEAM Analysis Tool 
Python client for MariTEAM web service.


### Install
`pip install mast-indecol`


### API Documentation
https://mariteam-analytics-tools.readthedocs.io

from typing import Optional
from pydantic import BaseModel
from enum import Enum


class ShipType(Enum):
    """
    Supported ship types.

    Attributes
    ----------
    other_offshore: 'other offshore',
    offshore_supply: 'offshore supply',
    passenger: 'passenger',
    oil: 'oil',
    general_cargo: 'general cargo',
    other_cargo: 'other cargo',
    other_bulk: 'other bulk',
    refrigerated_cargo: 'refrigerated cargo',
    other_liquids: 'other liquids',
    bulk_dry: 'bulk dry',
    liquefied_gas: 'liquefied gas',
    ro_ro: 'ro-ro',
    chemical: 'chemical',
    container: 'container'
    """

    other_offshore = 'other offshore',
    offshore_supply = 'offshore supply',
    passenger = 'passenger',
    oil = 'oil',
    general_cargo = 'general cargo',
    other_cargo = 'other cargo',
    other_bulk = 'other bulk',
    refrigerated_cargo = 'refrigerated cargo',
    other_liquids = 'other liquids',
    bulk_dry = 'bulk dry',
    liquefied_gas = 'liquefied gas',
    ro_ro = 'ro-ro',
    chemical = 'chemical',
    container = 'container'


class Ship(BaseModel):
    """
    Representation of Ship

    Parameters
    ----------
    mmsi: int
        MMSI of ship.
    imo_number: int
        IMO of ship.
    name: str
        Name of ship.
    aux_engine_kw: int
        Auxiliary engine power in kW.
    aux_engine_stroke: int
        Number of strokes of auxiliary engine.
    breadth: int
        Ship breadth.
    built: str
        Year the ship was build in format 'YYYY-MM'.
    draught: float
        Ship draught.
    dwt: int
        Ship deadweight tonnage.
    fuel_type: str
        Ship fuel type.
    gross: int
        TODO
    ldt: int
        Ship light displacement.
    length: float
        Ship length.
    length_bp: float
        Ship length between perpendiculars.
    main_engine_cyl: int
        Number of main engine cylinders.
    main_engine_kw: int
        Main engine power in kW.
    main_engine_model: str
        Main engine model
    main_engine_rpm: int
        Main engine rotation per minute.
    main_engine_stroke: int
        Number of main engine strokes.
    propulsion: str
        TODO
    reefer_points: int
        TODO
    service_speed: int
        Ship service speed.
    ship_type: ShipType
        Ship type.
    ship_type_detailed: str
        Ship type in detail.
    teu: int
        Ship twenty-foot equivalent unit.
    filler: str
        Filler information.
    note: str
        User note for.
    """

    mmsi: int
    imo_number: Optional[int] = None
    name: Optional[str] = None
    aux_engine_kw: Optional[int] = None
    aux_engine_stroke: Optional[int] = None
    breadth: Optional[int] = None
    built: Optional[str] = None
    draught: Optional[float] = None
    dwt: Optional[int] = None
    fuel_type: Optional[str] = None
    gross: Optional[int] = None
    ldt: Optional[int] = None
    length: Optional[float] = None
    length_bp: Optional[float] = None
    main_engine_cyl: Optional[int] = None
    main_engine_kw: Optional[int] = None
    main_engine_model: Optional[str] = None
    main_engine_rpm: Optional[int] = None
    main_engine_stroke: Optional[int] = None
    propulsion: Optional[str] = None
    reefer_points: Optional[int] = None
    service_speed: Optional[int] = None
    ship_type: Optional[str] = None
    ship_type_detailed: Optional[str] = None
    teu: Optional[int] = None
    filler: Optional[str] = None
    note: Optional[str] = None

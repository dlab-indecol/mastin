from typing import Optional
from pydantic import BaseModel, Field


class OperationalProfile(BaseModel):
    """
    Model of OperationalProfile

    Parameters
    ----------
    id: int
        Id of the OP
    mmsi: int
        MMSI of ship the OP belongs to
    created: int
        Time the file was uploaded.
    origin: str
        User name who uploaded the file.
    note: str
        Arbitrary user description.
    filename: str
        Name of the original file.
    file_content_type: str
        Media content of the original file
    """
    id: int = Field(None,
                    title='Unique identifier of object',
                    description='Each Operational profile has'
                                ' unique identifier when stored in database.')
    mmsi: int = Field(None,
                      title='MMSI',
                      description='The MMSI of ship the Operational '
                                  'profile belong to.')
    created: int = Field(None,
                         title='Unixtimestap',
                         description='Time the file was uploaded.')
    origin: str = Field(None,
                        title='Creator of the file',
                        description='User name who uploaded the file.')
    note: Optional[str] = Field(None,
                                title='Note',
                                description='Arbitrary description.')
    filename: str = Field(None,
                          title='Filename',
                          description='Name of the original file.')
    file_content_type: str = Field(None,
                                   title='Media type',
                                   description='Media content of the'
                                               ' original file')

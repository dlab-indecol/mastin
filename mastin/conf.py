import os
import yaml
import appdirs
import logging.config


APPNAME = "mast"
APPAUTHOR = "Indecol"

# API_URL = 'http://localhost/api/v1'
API_URL = 'https://mast.mariteam.site:443/api/v1'
# API_URL = 'http://mariteam.org.ntnu.no/api/v1'
SHIPS_URL = f'{API_URL}/ships'
OPS_URL = f'{API_URL}/operational-profiles'

log_path = '.'
log_filename = 'mast_indecol'


# /home/<username>/.local/share/mast
USER_DATA_DIR = appdirs.user_data_dir(appname=APPNAME, appauthor=APPAUTHOR)
# /home/<username>/.config/mast-indecol.yaml
USER_CONF_FILE = os.path.join(appdirs.user_config_dir(APPNAME, APPAUTHOR),
                              ".mast-indecol.yaml")

if not os.path.isdir(USER_DATA_DIR):
    os.makedirs(USER_DATA_DIR)
USER_CONFIG_DIR = appdirs.user_config_dir()
if not os.path.isdir(USER_CONFIG_DIR):
    os.makedirs(USER_CONFIG_DIR)

user_conf = None
try:
    with open(USER_CONF_FILE, 'r') as config:
        user_conf = yaml.load(config, Loader=yaml.FullLoader)
        if API_URL in user_conf:
            API_URL = user_conf['API_URL']
except FileNotFoundError:
    pass

TOKEN_URL = f"{API_URL}/login/access-token"
MAST_AUTH_USERNAME = 'MAST_AUTH_USERNAME'
MAST_AUTH_PASSWORD = 'MAST_AUTH_PASSWORD'
MAST_USER_TOKEN_FILE = os.path.join(USER_CONFIG_DIR,
                                    '.mast_indecol_access_token.conf')

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'formatter': 'standard',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',  # Default is stderr
        },
    },
    'loggers': {
        '': {  # root logger
            'handlers': ['default'],
            'level': 'INFO',
            'propagate': False
        },
        'my.packg': {
            'handlers': ['default'],
            'level': 'WARNING',
            'propagate': False
        },
        '__main__': {  # if __name__ == '__main__'
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': False
        },
    }
}

logging.config.dictConfig(LOGGING_CONFIG)

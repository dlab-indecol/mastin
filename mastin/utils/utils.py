import random


# The "default" random is actually an instance of `random.Random()`.
# The CSPRNG version uses `SystemRandom()` and `os.urandom()` in turn.
# More at:
# https://realpython.com/python-random/#why-not-just-default-to-systemrandom
_sysrand = random.SystemRandom()


# TODO use only single 9 digit followed by 8 random digits
def random_mmsi() -> int:
    return int('999' + str(_sysrand.randint(100000, 999999)))


def header_with_token(access_token) -> dict:
    return {
        'accept': 'application/json',
        'Authorization': f'Bearer {access_token}'
    }

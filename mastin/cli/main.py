#!/usr/bin/env python

import argparse
import json
import os
import pprint
from datetime import datetime
from getpass import getpass
from pathlib import Path

from requests.exceptions import ConnectionError
from tabulate import tabulate

import mastin.services.operational_profiles as service_op
import mastin.services.ships as service_ships
from mastin.conf import USER_DATA_DIR
from mastin.services.auth import login, access_token
from mastin.ships import create_empty_ship, create_ship

examples = '''
mastin ships list                             # list all created ship
mastin ships remove 1234                      # remove ship with mmsi 1234
mastin update 1234 --length=12 --note='test'  # update ship with mmsi 1234
                                              change length and note
mastin ship createStandard container          # creates container standard ship
'''


def _ships_save_local(ship):
    # we can save either dict or Ship model
    mmsi = ship['mmsi'] if isinstance(ship, dict) else ship.mmsi
    data = ship if isinstance(ship, dict) else ship.dict()

    with open(os.path.join(USER_DATA_DIR, f'{mmsi}.json'), 'w') as fp:
        json.dump(data, fp)


def _ships_load_local(mmsi):
    try:
        with open(os.path.join(USER_DATA_DIR, f'{mmsi}.json'),
                  'r') as fp:
            try:
                ret = json.load(fp)
            except Exception as e:
                print(f'Can not load {mmsi}.json. Error: {e}')
            else:
                return ret
    except FileNotFoundError:
        print(f'Ship with MMSI: {mmsi} does not exists')
        exit(0)


def _login(args):
    password = getpass()
    try:
        login(username=args.username, password=password)
    except ConnectionError:
        print(f'Can not login. Looks like server is down.')
    except Exception as e:
        print(f'Can not login. Error: {e}')


def _ship_list_print_table(ships):
    mmsis = []
    names = []
    notes = []
    for ship in ships:
        if ship:
            mmsis.append(ship['mmsi'])
            names.append(ship['name'])
            notes.append(ship['note'])
    table = {'MMSI': mmsis, 'Name': names, 'Note': notes}
    print(tabulate(table, headers=table.keys()))


def _op_list_print_table(ops):
    ids = []
    mmsis = []
    created = []
    owners = []
    notes = []
    for op in ops:
        ids.append(op['id'])
        mmsis.append(op['mmsi'])
        created.append(datetime.fromtimestamp(int(op['created'])))
        owners.append(op['origin'])
        notes.append(op['note'])

    table = {'Id': ids, 'MMSI': mmsis, 'Created': created,
             'Origin': owners, 'Note': notes}
    print(tabulate(table, headers=table.keys()))


def ships_list(args):
    if args.remote:
        response = service_ships.get_ships(access_token=access_token())
        if response.status_code == 200:
            _ship_list_print_table(json.loads(response.content))
        else:
            print(f"Error: {response.status_code} {response.text}")
    else:
        ships = (_ships_load_local(int(os.path.splitext(filename)[0]))
                 for filename in os.listdir(USER_DATA_DIR)
                 if filename.endswith(".json"))
        _ship_list_print_table(ships)


def ships_remove(args):
    try:
        os.remove(os.path.join(USER_DATA_DIR, f'{args.mmsi}.json'))
    except FileNotFoundError:
        print(f'Ship with MMSI: {args.mmsi} does not exists')
        exit(0)


def ships_update(args):
    ship = _ships_load_local(args.mmsi)
    for arg in vars(args):
        if arg == 'mmsi' or arg == 'func':
            continue
        if getattr(args, arg):
            ship[arg] = getattr(args, arg)
    _ships_save_local(ship)


def ships_inspect(args):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(_ships_load_local(args.mmsi))


# export to json
def ships_export(args):
    ship = _ships_load_local(args.mmsi)
    filename = args.filename if args.filename else args.mmsi

    with open(f'{filename}.json', 'w') as fp:
        print(ship['mmsi'])
        json.dump(ship, fp)


# push to remote database
def ships_push(args):
    ship_in = _ships_load_local(args.mmsi)
    response = service_ships.create_ship(access_token=access_token(),
                                         ship_in=ship_in)
    if response.status_code == 200:
        print(f"{ship_in['mmsi']}")
    else:
        print(f"Can't push. Reason: {response.text}")


# create ship using conf file
def ships_create(args):
    try:
        with open(args.conf_file, 'r') as fp:
            params = json.load(fp)
    except FileNotFoundError:
        print(f'File {args.conf_file} does not exists')
        exit(0)
    else:
        ship = create_ship(**params)
        _ships_save_local(ship)
        print(ship.mmsi)


# create ship with only MMSI specified
def ships_create_empty(args):
    assert args
    ship = create_empty_ship()
    _ships_save_local(ship)
    print(f'{ship.mmsi}')


# create ship based on median of segment group
def ships_create_standard(args):
    assert args
    print('Not implemented yet')


def ops_list(args):
    response = service_op.get_ops(access_token=access_token(), mmsi=args.mmsi)
    if response.status_code == 200:
        _op_list_print_table(json.loads(response.content))
    else:
        print(f"Error: {response.status_code} {response.text}")


# push to remote database
def ops_push(args):
    if not Path(args.OP_file).is_file():
        print(f'The file {args.OP_file} does not exists!')
        return
    if Path(args.OP_file).suffix != ".json":
        print('File must be json format!')
        return
    note = args.note if args.note else ' '
    response = service_op.create_ops(access_token=access_token(),
                                     file=args.OP_file,
                                     mmsi=args.mmsi,
                                     note=note)
    if response.status_code == 201:
        print(f"{args.mmsi}")
    else:
        print(f"Can't push Operational profile. Reason: {response.text}")


# pull from remote db
def ops_pull(args):
    response = service_op.get_ops_file(access_token=access_token(),
                                       id=args.id)
    if response.status_code == 200:
        content = json.loads(response.content)
        with open(content['filename'], 'w', encoding='utf-8') as f:
            json.dump(content['file_blob'], f,
                      ensure_ascii=False, indent=4)
    else:
        print(f"Can't pull Operational profile. Reason: {response.text}")


# pull from remote db
def ops_inspect(args):
    response = service_op.get_ops_inspect(access_token=access_token(),
                                          id=args.id, records=args.records)
    if response.status_code == 200:
        content = json.loads(response.content)
        x = json.loads(content)
        print(tabulate([item.values() for item in x],
                       headers=x[0].keys(),
                       tablefmt="plain"))
    else:
        print(f"Can't pull Operational profile. Reason: {response.text}")


def main():
    parser = argparse.ArgumentParser(
        epilog=examples,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    subparsers = parser.add_subparsers(title='Management commands',
                                       help='commands')

    # Login
    login_parser = subparsers.add_parser(
        'login', help='Authenticate user')
    login_parser.add_argument('username', action='store', help="User name")
    login_parser.set_defaults(func=_login)

    # A ships commands
    ship_parser = subparsers.add_parser(
        'ships', help='Manage ships')

    ship_sp = ship_parser.add_subparsers(title='Commands', help='commands')

    # Ship list
    ship_list = ship_sp.add_parser('list', help='List ships')
    ship_list.add_argument('--remote', action='store_true',
                           help='show remotely stored ships')
    ship_list.set_defaults(func=ships_list)

    # Ship remove
    ship_remove = ship_sp.add_parser('remove',
                                     help='Remove ship with specified MMSI')
    ship_remove.add_argument('mmsi', action='store', help="Ship's MMSI")
    ship_remove.set_defaults(func=ships_remove)

    # Ship update
    ship_update = ship_sp.add_parser(
        'update',
        help='Update ship with specified parameters')
    ship_update.add_argument('mmsi', action='store', help="Ship's MMSI")

    ship_update.add_argument('--imo_number', type=int, help='IMO of ship')
    ship_update.add_argument('--name', type=str, help=' Name of ship.')
    ship_update.add_argument('--aux_engine_kw', type=int,
                             help='Auxiliary engine power in kW.')
    ship_update.add_argument('--aux_engine_stroke', type=str,
                             help='Number of strokes of auxiliary engine.')
    ship_update.add_argument('--breadth', type=int, help='Ship breadth.')
    ship_update.add_argument('--built', type=str,
                             help="Year ship was build in format 'YYYY-MM'")
    ship_update.add_argument('--draught', type=float, help=' Ship draught.')
    ship_update.add_argument('--dwt', type=int,
                             help=' Ship deadweight tonnage.')
    ship_update.add_argument('--fuel_type', type=str, help=' Ship fuel type.')
    ship_update.add_argument('--gross', type=int, help='')
    ship_update.add_argument('--ldt', type=int,
                             help='Ship light displacement.')
    ship_update.add_argument('--length', type=float, help='Ship length.')
    ship_update.add_argument('--length_bp', type=float,
                             help='Ship length between perpendiculars.')
    ship_update.add_argument('--main_engine_cyl', type=int,
                             help='Number of main engine cylinders.')
    ship_update.add_argument('--main_engine_kw', type=int,
                             help='Main engine power in kW.')
    ship_update.add_argument('--main_engine_model', type=str,
                             help='Main engine model')
    ship_update.add_argument('--main_engine_rpm', type=int,
                             help='Main engine rotation per minute.')
    ship_update.add_argument('--main_engine_stroke', type=int,
                             help=' Number of main engine strokes.')
    ship_update.add_argument('--propulsion', type=str, help='')
    ship_update.add_argument('--reefer_points', type=int, help='')
    ship_update.add_argument('--service_speed', type=int,
                             help='Ship service speed.')
    ship_update.add_argument('--ship_type', type=str, help='Ship type.')
    ship_update.add_argument('--ship_type_detailed', type=str,
                             help='Ship type in detail.')
    ship_update.add_argument('--teu', type=int,
                             help='Ship twenty-foot equivalent unit.')
    ship_update.add_argument('--note', type=str, help='User note for.')
    ship_update.set_defaults(func=ships_update)

    # Ship inspect
    ship_inspect = ship_sp.add_parser('inspect',
                                      help='Inspect ship with specified MMSI')
    ship_inspect.add_argument('mmsi', action='store', help="Ship's MMSI")
    ship_inspect.set_defaults(func=ships_inspect)

    # Ship export
    ship_export = ship_sp.add_parser('export',
                                     help='Export ship to json file')
    ship_export.add_argument('mmsi', action='store', help="Ship's MMSI")
    ship_export.add_argument('--filename', action='store',
                             help="Output filename. "
                                  "If omitted the mmsi will be used.",
                             default=None)
    ship_export.set_defaults(func=ships_export)

    # Ship push
    ship_push = ship_sp.add_parser('push',
                                   help='Push ship config to remote database')
    ship_push.add_argument('mmsi', action='store', help="Ship's MMSI")
    ship_push.set_defaults(func=ships_push)

    # Ship create with config file
    ship_create = ship_sp.add_parser('create',
                                     help='Create ships based on ship conf '
                                          'json '
                                          'file with specific attributes.')
    ship_create.add_argument('conf_file', action='store',
                             help="json conf file")
    ship_create.set_defaults(func=ships_create)

    # Ship create empty
    ship_create_empty = ship_sp.add_parser('createEmpty',
                                           help='Create ships with only mmsi '
                                                'specified. All other '
                                                'attributes '
                                                'are set to None.')
    ship_create_empty.set_defaults(func=ships_create_empty)

    # Ship create standard
    ship_create_standard = ship_sp.add_parser('createStandard',
                                              help='Create standard ship per '
                                                   'segment (ship type) with '
                                                   'all values calculated'
                                                   ' as median.')

    ship_create_standard.add_argument('ship_type', action='store',
                                      help="Ship type")
    ship_create_standard.set_defaults(func=ships_create_standard)

    # Operational profiles command
    op_parser = subparsers.add_parser(
        'ops', help='Manage Operational profiles')

    op_sp = op_parser.add_subparsers(title='Commands', help='commands')

    # OP list
    op_list = op_sp.add_parser('list', help='List Operational Profiles '
                                            'from remote database.')
    op_list.add_argument('mmsi', action='store',
                         help="All OPs for the MMSI will be listed")
    op_list.set_defaults(func=ops_list)

    # OP inspect
    op_inspect = op_sp.add_parser('inspect',
                                  help='Inspect Operational Profile')
    op_inspect.add_argument('id', action='store',
                            help="Id of the Operational profile.")
    op_inspect.add_argument('--records', action='store', type=int, default=5,
                            help="How many records to show.")
    op_inspect.set_defaults(func=ops_inspect)

    # OPs push
    op_push = op_sp.add_parser('push',
                               help='Push Operational Profiles'
                                    ' to remote database')
    op_push.add_argument('mmsi', action='store',
                         help="MMSI of shipt the Operational file belongs to.")
    op_push.add_argument('OP_file', action='store',
                         help="either json or csv OP file")
    op_push.add_argument('--note', action='store',
                         help="Optional note.")
    op_push.set_defaults(func=ops_push)

    # OP file pull
    op_pull = op_sp.add_parser('pull', help='Pull Operational Profiles '
                                            'from remote database.')
    op_pull.add_argument('id', action='store',
                         help="Id of the Operational profile.")
    op_pull.set_defaults(func=ops_pull)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()

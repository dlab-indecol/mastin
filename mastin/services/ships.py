import requests
import json

from mastin.conf import SHIPS_URL
from mastin.utils.utils import header_with_token


def get_ships(access_token, mmsi=None):
    if mmsi:
        return requests.get(f"{SHIPS_URL}/{mmsi}",
                            headers=header_with_token(
                                access_token=access_token))
    else:
        return requests.get(f"{SHIPS_URL}/",
                            headers=header_with_token(
                                access_token=access_token))


def create_ship(access_token, ship_in):
    return requests.post(f"{SHIPS_URL}/",
                         data=json.dumps(ship_in),
                         headers=header_with_token(
                             access_token=access_token))


def delete_ship(access_token, mmsi):
    return requests.delete(f"{SHIPS_URL}/{mmsi}",
                           headers=header_with_token(
                               access_token=access_token))

import logging
import os

import requests
import yaml

from mastin.conf import (user_conf, TOKEN_URL, MAST_USER_TOKEN_FILE,
                         MAST_AUTH_USERNAME, MAST_AUTH_PASSWORD)


def login(username: str = None, password: str = None) -> bool:
    if username:
        username = username
    elif user_conf and 'username' in user_conf:
        logging.info('Reading username from config file.')
        username = user_conf['username']
    elif MAST_AUTH_USERNAME in os.environ:
        logging.info('Reading username from environment variable.')
        username = os.environ[MAST_AUTH_USERNAME]
    else:
        logging.error('No username provided')
        print('No username provided')
    if password:
        password = password
    elif user_conf and 'password' in user_conf:
        logging.info('Reading password from config file.')
        password = user_conf['password']
    elif MAST_AUTH_PASSWORD in os.environ:
        logging.info('Reading username from environment variable.')
        password = os.environ[MAST_AUTH_PASSWORD]
    else:
        logging.error('No password provided')
        print('No password provided')

    data = {
        'grant_type': '',
        'username': username,
        'password': password,
        'scope': '',
        'client_id': '',
        'client_secret': ''
    }

    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }

    print(TOKEN_URL)
    response = requests.post(TOKEN_URL, data=data, headers=headers)

    if response.status_code != 200:
        raise RuntimeError(f'Can not authenticate {response.text}')

    response = response.json()

    if 'access_token' not in response:
        raise RuntimeError("Can't retrieve access token. "
                           "Are your credentials valid?")

    logging.info('Login successful')
    print('Login successful')

    try:
        with open(MAST_USER_TOKEN_FILE, 'w') as token_conf:
            try:
                yaml.dump(response, token_conf)
            except yaml.YAMLError as exc:
                print(exc)
    except FileNotFoundError:
        pass

    return True


def access_token() -> str:
    with open(MAST_USER_TOKEN_FILE, 'r') as file:
        conf = yaml.load(file, Loader=yaml.FullLoader)
        if 'access_token' in conf:
            return conf['access_token']

        print('Please login')

import requests

from mastin.conf import OPS_URL
from mastin.utils.utils import header_with_token


def get_ops(access_token, mmsi: int):
    return requests.get(f"{OPS_URL}/{mmsi}",
                        headers=header_with_token(
                            access_token=access_token))


def get_ops_file(access_token, id: int):
    return requests.get(f"{OPS_URL}/files/{id}",
                        headers=header_with_token(
                            access_token=access_token))


def get_ops_inspect(access_token, id: int, records: int):
    return requests.get(f"{OPS_URL}/inspect/{id}",
                        params=({'records': records}),
                        headers=header_with_token(
                            access_token=access_token))


def create_ops(access_token, file, mmsi, note):
    return requests.post(f"{OPS_URL}/",
                         files={'file': open(file, 'rb')},
                         data={'mmsi': mmsi, 'note': note},
                         headers=header_with_token(
                             access_token=access_token))


# def delete_ops(access_token, mmsi):
#     return requests.delete(f"{OPS_URL}/{mmsi}",
#                            headers=header_with_token(
#                                access_token=access_token))

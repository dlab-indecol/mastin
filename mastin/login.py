"""
Login management
"""

from mastin.services.auth import login


def authentificate(username: str, password: str) -> bool:
    """
    Login to MariTEAM Indecol Web service.

    Parameters
    ----------
    username: str
        email of the user
    password: str
        user password

    Returns
    -------
    bool
        True on success.

    """

    return login(username=username, password=password)

"""
Manage Operational profiles (OPs).

Operational Profile is TODO

Each Operational Profile must have MMSI which correspond to
operational_profile MMSI.
However there can be multiple OPs with same MMSI so the user can modify
some fields for different use cases.

The OP is stored with metadata and the file itself.
"""

import json
from typing import List

from mastin.models.operational_profile import OperationalProfile
from mastin.services import operational_profiles, auth


def get_operational_profile(mmsi: int) -> List[OperationalProfile]:
    """
    Get list of operational profiles with same mmsi from database.

    Parameters
    ----------
    mmsi: int
        mmsi of the operational_profiles

    Returns
    -------
    operational_profiles: List[OperationalProfile]

    Raises
    ------
    RuntimeError
        If operational_profile with specified mmsi can not be retrieved
        from server.
    """
    response = operational_profiles.get_ops(
        access_token=auth.access_token(), mmsi=mmsi)
    if response.status_code == 200:
        response = json.loads(response.content)
        return [OperationalProfile(**item) for item in response]
    else:
        raise RuntimeError(f'Can not get OperationalProfile with {mmsi}. '
                           f'Server respond: {response.text}')


def download_operational_profile(id: int) -> str:
    """
    Download OperationalProfile as json file from database.

    Parameters
    ----------
    id: int
        id of the OperationalProfile

    Returns
    -------
    filename: name of the downloaded file

    Raises
    ------
    RuntimeError
        If operational_profile with specified id
         can not be retrieved from server.
    """
    response = operational_profiles.get_ops_file(
        access_token=auth.access_token(), id=id)
    if response.status_code == 200:
        content = json.loads(response.content)
        with open(content['filename'], 'w', encoding='utf-8') as f:
            json.dump(content['file_blob'], f,
                      ensure_ascii=False, indent=4)
        return content['filename']
    else:
        raise RuntimeError(f'Can not get OperationalProfile with {id}. '
                           f'Server respond: {response.text}')


def create_operational_profile(file: str, mmsi: int, note: str = '') \
        -> int:
    """
    Create Operational Profile.

    Parameters
    ----------
    mmsi: int
        MMSI of the shipt the OP belongs to
    file: str
        path to the file
    note: str
        Usr note. default to empty string ''

    Returns
    -------
    int
        MMSI fo the Operational profile
    """

    response = operational_profiles.create_ops(
        access_token=auth.access_token(), file=file, mmsi=mmsi, note=note)

    if response.status_code == 201:
        return mmsi
    else:
        raise RuntimeError(f'Can not save operational_profile. '
                           f'Server respond: {response.text}')


def inspect_operational_profile(id: int, records=5) -> List[dict]:
    """
    Inspect Operational profile.

    Parameters
    ----------
    id: int
        id of the :py:class:`OperationalProfile`
    records: int
        How many records of the data to show.
        Default 5

    Returns
    -------
    list[dict]:
        list of dict records

    Raises
    ------
    RuntimeError
        If operational_profile with specified id
         can not be retrieved from server.
    """
    response = operational_profiles.get_ops_inspect(
        access_token=auth.access_token(),
        id=id,
        records=records)
    if response.status_code == 200:
        content = json.loads(response.content)
        return json.loads(content)
    else:
        print(f"Can't pull Operational profile. Reason: {response.text}")
